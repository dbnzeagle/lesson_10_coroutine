package com.example.myapplication

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface Api {
    @GET("/recipes")
    fun fetchData(): Call<Data>
}