package com.example.myapplication

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import java.net.URL

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val api = Network.provideRetrofit()
        button.setOnClickListener {

            CoroutineScope(Dispatchers.Main).launch {
                var bitmap: Bitmap? = null
                var data: Data? = null
                try {
                    data = Repository().getData()
                } catch (e: Throwable) {
                    Log.e("Error", e.message)
                }
                Log.e("Data", data.toString())
                withContext(Dispatchers.IO) {
                    calculate()
                    Log.e("Я тут", "!")
                    bitmap = loadImageFromNet()
                }
                if (bitmap != null)
                    imageView.setImageBitmap(bitmap)
            }
        }
    }

    fun calculate() = runBlocking {
        val jobs = List(100_000) {
            launch {
                delay(1000L)
                Log.w("Dot", ".")
            }
        }
        jobs.forEach { it.join() }
    }

    fun loadImageFromNet(): Bitmap {
        val url =
            URL("https://images.freeimages.com/images/small-previews/7e9/ladybird-1367182.jpg")
        return BitmapFactory.decodeStream(url.openConnection().getInputStream())
    }


    override fun onDestroy() {
        super.onDestroy()

    }
}
