package com.example.myapplication

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class Repository {

    suspend fun getData(): Data {
        val api = Network.provideRetrofit()
        return suspendCoroutine { continuation ->
            CoroutineScope(Dispatchers.IO).launch {
                api.fetchData().enqueue(object : Callback<Data> {
                    override fun onFailure(call: Call<Data>, t: Throwable) {
                        continuation.resumeWithException(t)
                    }

                    override fun onResponse(call: Call<Data>, response: Response<Data>) {
                        if (response.body()?.list != null)
                            continuation.resume(response.body()!!)
                        else
                            continuation.resumeWithException(Throwable("Null"))
                    }
                })
            }
        }
    }
}